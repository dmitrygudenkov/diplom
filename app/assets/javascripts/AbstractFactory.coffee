class @AbstractFactory
  constructor: (@$http, @model) ->
    @all()

  all: ->
    @$http.get '/' + @model + '.json'

  show: (id) ->
    @$http.get '/' + @model + '/' + id + '.json'

  create: (data) ->
    @$http.post '/' + @model + '.json', data

  update: (data) ->
    @$http.put '/' + @model + '/' + data.id + '.json', data

  destroy: (data) ->
    @$http['delete'] '/' + @model + '/' + data.id + '.json'