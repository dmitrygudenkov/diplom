class @AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Model, @keysCount) ->
    @$scope = $scope

  add: =>
    console.log @data
    keysCount = 0
    for key, value of @data
      keysCount++
      if value != 0
        unless value
          swal 'Ошибка', 'Надо заполнить все поля!', 'error'
          return
    if keysCount != @keysCount
      swal 'Ошибка', 'Надо заполнить все поля!', 'error'
      return
    @Model.create @data
      .then (success) =>
        @$uibModalInstance.close(success.data)
      .then (error) =>
        @$uibModalInstance.close()
      

  cancel: =>
    @$uibModalInstance.dismiss()