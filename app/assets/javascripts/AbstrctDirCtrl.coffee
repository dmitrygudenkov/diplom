class @AbstractDirCtrl
  constructor: ($scope, @Model, @ctrlName) ->
    @$scope = $scope
    do @load_data

  load_data: =>
    @Model.all()
      .then (response) =>
        @data = response.data
        @$scope.$digest() unless @$scope.$$phase

  save: (elem) =>
    @Model.update elem
      .then (success) =>
        console.log success
      .then (error) =>
        console.log error

  destroy: (elem) =>
    swal {
      title: 'Вы уверены?'
      text: 'Вы действительно хотите удалить ' + elem.name + '?'
      type: 'warning'
      showCancelButton: true
      confirmButtonClass: 'btn-warning'
      confirmButtonText: 'Да'
      closeOnConfirm: true
      closeOnCancel: true
    }, (isConfirm) =>
      if isConfirm
        @Model.destroy elem
          .then (success) =>
            i = @data.findIndex (e) =>
              if e.id == elem.id
                e
            @data.splice i, 1
          .then (error) =>
            console.log error

  showWindow: =>
    modalInstace = @$uibModal.open(
      animation: true
      templateUrl: @ctrlName + 's/windows/_add' + @ctrlName[0].toUpperCase() + @ctrlName.substring(1) + '.html'
      controller: 'Add' + @ctrlName[0].toUpperCase() + @ctrlName.substring(1) + 'WindowCtrl as ctrl'
      size: 'lg')
    modalInstace.result.then (response) =>
      if response then @data.push response