angular.module('skyscraper', [
  'ui.router'
  'templates'
  'xeditable'
  'ui.bootstrap'
  'ngAnimate'
  'ngMaterial'
  'ngResource'
]).run((editableOptions) ->
  editableOptions.theme = 'bs3'
).config ($resourceProvider, $mdThemingProvider) ->
  $resourceProvider.defaults.stripTrailingSlashes = false;
  # $mdThemingProvider.theme()
  $mdThemingProvider.theme('default')
    .primaryPalette 'orange'
    .accentPalette  'green'