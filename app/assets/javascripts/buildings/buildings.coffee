class BuildingCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'buildings'


angular.module('skyscraper')
  .factory 'Building', [
    '$http'
    ($http) ->
      new BuildingCRUD($http)
]