class BuildingsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @Building) ->
    super $scope, @Building, 'building'

angular.module('skyscraper').controller 'BuildingsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  'Building'
  BuildingsCtrl
]