class AddBuildingWindowCtrl extends AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Building) ->
    super $scope, @$uibModalInstance, @Building, 4

angular.module('skyscraper').controller 'AddBuildingWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Building'
  AddBuildingWindowCtrl
]