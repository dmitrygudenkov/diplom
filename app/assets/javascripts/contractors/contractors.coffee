class ContractorCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'contractors'


angular.module('skyscraper')
  .factory 'Contractor', [
    '$http'
    ($http) ->
      new ContractorCRUD($http)
]