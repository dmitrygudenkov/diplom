class ContractorsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @Contractor) ->
    super $scope, @Contractor, 'contractor'

angular.module('skyscraper').controller 'ContractorsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  'Contractor'
  ContractorsCtrl
]