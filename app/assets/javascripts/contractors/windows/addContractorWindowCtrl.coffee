class AddContractorWindowCtrl extends AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Contractor) ->
    super $scope, @$uibModalInstance, @Contractor, 1

angular.module('skyscraper').controller 'AddContractorWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Contractor'
  AddContractorWindowCtrl
]