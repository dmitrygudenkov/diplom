class EventCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'events'

  find: (id) ->
    @$http.get '/' + @model + '?find=' + id

angular.module('skyscraper')
  .factory 'Event', [
    '$http'
    ($http) ->
      new EventCRUD($http)
]