class ShowEventsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @$stateParams, @Event, @Work, @Worktype) ->
    super $scope, @Event, 'event'

  load_data: =>
    @Event.find(@$stateParams.id)
      .then (success) =>
        @data = success.data
        @Work.find(@$stateParams.id)
      .then (success) =>
        @works = success.data
        @Worktype.all()
      .then (success) =>
        @worktypes = success.data
        @$scope.$digest() unless @$scope.$$phase
        do @bind_data

  bind_data: =>
    @works.forEach (w) =>
      w.worktype = @worktypes.find (wt) =>
        if wt.id == w.worktype_id
          wt
    @data.forEach (d) =>
      @bind_data_for_row d

  bind_data_for_row: (r) =>
      r.work = @works.find (w) =>
        if w.id == r.work_id
          return w
      r.prev_work = @works.find (w) =>
        if w.id == r.prev_work_id
          return w

  save: (row) =>
    @Event.update row
      .then (success) =>
        @bind_data_for_row row
        

angular.module('skyscraper').controller 'ShowEventsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  '$stateParams'
  'Event'
  'Work'
  'Worktype'
  ShowEventsCtrl
]