class MachinekitcountCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'machinekitcounts'

  find: (id) ->
    @$http.get '/' + @model + '?find=' + id

  group_all: ->
    @$http.get '/' + @model + '?sql=true'


angular.module('skyscraper')
  .factory 'Machinekitcount', [
    '$http'
    ($http) ->
      new MachinekitcountCRUD($http)
]