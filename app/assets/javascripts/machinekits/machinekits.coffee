class MachinekitCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'machinekits'


angular.module('skyscraper')
  .factory 'Machinekit', [
    '$http'
    ($http) ->
      new MachinekitCRUD($http)
]