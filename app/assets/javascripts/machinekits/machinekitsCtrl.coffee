class MachinekitsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @Machinekit) ->
    super $scope, @Machinekit, 'machinekit'

  editKit: (machinekit) ->
    modalInstace = @$uibModal.open(
      animation: true
      resolve: {
        machinekit: -> return machinekit
      }
      templateUrl: 'machinekits/windows/_showMachinekit.html'
      controller: 'ShowMachinekitWindowCtrl as ctrl'
      size: 'lg')

angular.module('skyscraper').controller 'MachinekitsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  'Machinekit'
  MachinekitsCtrl
]