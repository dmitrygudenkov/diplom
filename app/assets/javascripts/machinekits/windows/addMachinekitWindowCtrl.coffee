class AddMachinekitWindowCtrl extends AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Machinekit) ->
    super $scope, @$uibModalInstance, @Machinekit, 1

angular.module('skyscraper').controller 'AddMachinekitWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Machinekit'
  AddMachinekitWindowCtrl
]