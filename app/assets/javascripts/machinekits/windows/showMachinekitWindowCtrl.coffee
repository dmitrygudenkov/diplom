class @ShowMachinekitWindowCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModalInstance, @Machinekitcount, @Machine, @machinekit) ->
    super $scope, @Machinekitcount, 'machinekitcount'
    @$scope = $scope
    do @load_data

  load_data: =>
    @Machinekitcount.find(@machinekit.id)
      .then (response) =>
        @data = response.data
        @Machine.all()
      .then (response) =>
        @machines = response.data
        @$scope.$digest() unless @$scope.$$phase

  showName: (id) =>
    if @machines
      machine = @machines.find (m) =>
        if m.id == id
          m
      machine.name

  add: =>
    machine = 
      machinekit_id:  @machinekit.id
      machine_id:     @machines[0].id
      count:          1
    @Machinekitcount.create machine
      .then (success) =>
        @data.push success.data




angular.module('skyscraper').controller 'ShowMachinekitWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Machinekitcount'
  'Machine'
  'machinekit'
  ShowMachinekitWindowCtrl
]