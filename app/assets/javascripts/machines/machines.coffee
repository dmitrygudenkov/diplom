class MachineCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'machines'


angular.module('skyscraper')
  .factory 'Machine', [
    '$http'
    ($http) ->
      new MachineCRUD($http)
]