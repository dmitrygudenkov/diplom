class MachinesCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @Machine) ->
    super $scope, @Machine, 'machine'

angular.module('skyscraper').controller 'MachinesCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  'Machine'
  MachinesCtrl
]