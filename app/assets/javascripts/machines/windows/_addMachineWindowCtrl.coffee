class AddMachineWindowCtrl extends AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Machine) ->
    super $scope, @$uibModalInstance, @Machine, 2

angular.module('skyscraper').controller 'AddMachineWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Machine'
  AddMachineWindowCtrl
]