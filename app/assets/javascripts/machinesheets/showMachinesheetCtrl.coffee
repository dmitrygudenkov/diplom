class ShowMachinesheetsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @$state, @$stateParams, @Timetable, @Machinekitcount, @Machine) ->
    super $scope, @Timetable, 'timetable'

  load_data: =>
    @Timetable.get_works(@$stateParams.id)
      .then (response) =>
        @works = response.data
        @Machinekitcount.group_all()
      .then (response) =>
        @machinekits = response.data
        @Machine.all()
      .then (response) =>
        @machines = response.data
        @$scope.$digest() unless @$scope.$$phase
        do @s_works

  go_to: (state) ->
    @$state.go state, id: @$stateParams.id, id1: @$stateParams.id

  s_works: =>
    @works.forEach (w) =>
      @get_start_day w
      @add_machines_to_works w
    @cal_machines = []
    @works.forEach (w) =>
      w.machines.forEach (m) =>
        coincedence = false
        @cal_machines.forEach (tm) =>
          if m.id == tm.id
            coincedence = true
            tm.dates.push { start_date: w.start_date, end_date: w.start_date + w.duration } 
            tm.counts.push m.count
        if coincedence == false
          m.dates = []
          m.counts = []
          m.dates.push { start_date: w.start_date, end_date: w.start_date + w.duration } 
          m.counts.push m.count
          @cal_machines.push m

  get_start_day: (work) =>
      if work.prev_work_id == null
        work.start_date = 1
      else
        @works.forEach (w) =>
          if work.prev_work_id == w.id
            unless w.start_date == undefined
              work.start_date = w.start_date + w.duration
            else
              @get_start_day w
              work.start_date = w.start_date + w.duration

  add_machines_to_works: (work) =>
    work.machines = []
    @machinekits.forEach (mk) =>
      if mk.machinekit_id == work.machinekit_id
        @machines.forEach (m) =>
          if m.id == mk.machine_id
            work.machines.push { id: m.id, name: m.name, perfomance: m.perfomance, count: mk.count }
    work


angular.module('skyscraper').controller 'ShowMachinesheetsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  '$state'
  '$stateParams'
  'Timetable'
  'Machinekitcount'
  'Machine'
  ShowMachinesheetsCtrl
]