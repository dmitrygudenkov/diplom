class ShowMaterialcalculationsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @$stateParams, @Worktype, @Work, @Material, @Materialkit, @Materialkitcount) ->
    super $scope, @Work, 'work'

  load_data: =>
    @Worktype.all()
      .then (success) =>
        @worktypes = success.data
        @Materialkitcount.group_all()
      .then (success) =>
        @materialkits = success.data
        @Material.all()
      .then (success) =>
        @materials = success.data
        @Work.find(@$stateParams.id)
      .then (success) =>
        @data = success.data
        @$scope.$digest() unless @$scope.$$phase
        do @bind_data

  bind_data: =>
    @data.forEach (d) =>
      @bind_data_for_row d
    console.log @data

  bind_data_for_row: (r) =>
    r.worktype = @worktypes.find (wt) =>
      if r.worktype_id == wt.id
        wt
    r.materials = []
    @materialkits.forEach (mk) =>
      if mk.materialkit_id == r.materialkit_id
        @materials.forEach (m) =>
          if m.id == mk.material_id
            r.materials.push { name: m.name, metric: m.metric, norm: mk.norm }
    

angular.module('skyscraper').controller 'ShowMaterialcalculationsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  '$stateParams'
  'Worktype'
  'Work'
  'Material'
  'Materialkit'
  'Materialkitcount'
  ShowMaterialcalculationsCtrl
]