class MaterialkitcountCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'materialkitcounts'

  find: (id) ->
    @$http.get '/' + @model + '?find=' + id

  group_all: ->
    @$http.get '/' + @model + '?sql=true'


angular.module('skyscraper')
  .factory 'Materialkitcount', [
    '$http'
    ($http) ->
      new MaterialkitcountCRUD($http)
]