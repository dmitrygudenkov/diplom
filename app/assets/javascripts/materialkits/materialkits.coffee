class MaterialkitCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'materialkits'


angular.module('skyscraper')
  .factory 'Materialkit', [
    '$http'
    ($http) ->
      new MaterialkitCRUD($http)
]