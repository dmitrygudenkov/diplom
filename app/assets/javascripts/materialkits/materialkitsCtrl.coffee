class MaterialkitsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @Materialkit) ->
    super $scope, @Materialkit, 'materialkit'

  editKit: (materialkit) ->
    modalInstace = @$uibModal.open(
      animation: true
      resolve: {
        materialkit: -> return materialkit
      }
      templateUrl: 'materialkits/windows/_showMaterialkit.html'
      controller: 'ShowMaterialkitWindowCtrl as ctrl'
      size: 'lg')

angular.module('skyscraper').controller 'MaterialkitsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  'Materialkit'
  MaterialkitsCtrl
]