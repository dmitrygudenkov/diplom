class AddMaterialkitWindowCtrl extends AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Materialkit) ->
    super $scope, @$uibModalInstance, @Materialkit, 1

angular.module('skyscraper').controller 'AddMaterialkitWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Materialkit'
  AddMaterialkitWindowCtrl
]