class @ShowMaterialkitWindowCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModalInstance, @Materialkitcount, @Material, @materialkit) ->
    super $scope, @Materialkitcount, 'materialkitcount'
    @$scope = $scope
    do @load_data

  load_data: =>
    @Materialkitcount.find(@materialkit.id)
      .then (response) =>
        @data = response.data
        @Material.all()
      .then (response) =>
        @materials = response.data
        @$scope.$digest() unless @$scope.$$phase

  showName: (id) =>
    if @materials
      material = @materials.find (m) =>
        if m.id == id
          m
      material.name

  add: =>
    material = 
      materialkit_id:  @materialkit.id
      material_id:     @materials[0].id
      norm:            1
    @Materialkitcount.create material
      .then (success) =>
        @data.push success.data




angular.module('skyscraper').controller 'ShowMaterialkitWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Materialkitcount'
  'Material'
  'materialkit'
  ShowMaterialkitWindowCtrl
]