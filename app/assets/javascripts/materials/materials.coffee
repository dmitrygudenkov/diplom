class MaterialCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'materials'


angular.module('skyscraper')
  .factory 'Material', [
    '$http'
    ($http) ->
      new MaterialCRUD($http)
]