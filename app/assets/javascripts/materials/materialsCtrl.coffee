class MaterialsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @Material) ->
    super $scope, @Material, 'material'

angular.module('skyscraper').controller 'MaterialsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  'Material'
  MaterialsCtrl
]