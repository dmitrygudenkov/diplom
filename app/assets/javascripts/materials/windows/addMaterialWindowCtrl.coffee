class AddMaterialWindowCtrl extends AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Material) ->
    super $scope, @$uibModalInstance, @Material, 2

angular.module('skyscraper').controller 'AddMaterialWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Material'
  AddMaterialWindowCtrl
]