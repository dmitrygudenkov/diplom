class ShowMaterialsheetsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @$state, @$stateParams, @Timetable, @Materialkitcount, @Material) ->
    super $scope, @Timetable, 'timetable'

  load_data: =>
    @Timetable.get_works(@$stateParams.id)
      .then (response) =>
        @works = response.data
        @Materialkitcount.group_all()
      .then (response) =>
        @materialkits = response.data
        @Material.all()
      .then (response) =>
        @materials = response.data
        @$scope.$digest() unless @$scope.$$phase
        do @s_works

  go_to: (state) ->
    @$state.go state, id: @$stateParams.id, id1: @$stateParams.id

  s_works: =>
    @works.forEach (w) =>
      @get_start_day w
      @add_materials_to_works w
    @cal_materials = []
    @works.forEach (w) =>
      w.materials.forEach (m) =>
        coincedence = false
        @cal_materials.forEach (tm) =>
          if m.id == tm.id
            coincedence = true
            tm.dates.push { start_date: w.start_date, end_date: w.start_date + w.duration, volume: m.norm * (w.volume - w.success) } 
        if coincedence == false
          m.dates = []
          m.dates.push { start_date: w.start_date, end_date: w.start_date + w.duration, volume: m.norm * (w.volume - w.success) } 
          @cal_materials.push m
    console.log @cal_materials


  get_start_day: (work) =>
      if work.prev_work_id == null
        work.start_date = 1
      else
        @works.forEach (w) =>
          if work.prev_work_id == w.id
            unless w.start_date == undefined
              work.start_date = w.start_date + w.duration
            else
              @get_start_day w
              work.start_date = w.start_date + w.duration

  add_materials_to_works: (work) =>
    work.materials = []
    @materialkits.forEach (mk) =>
      if mk.materialkit_id == work.materialkit_id
        @materials.forEach (m) =>
          if m.id == mk.material_id
            work.materials.push { id: m.id, name: m.name, metric: m.metric, norm: mk.norm }
    work


angular.module('skyscraper').controller 'ShowMaterialsheetsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  '$state'
  '$stateParams'
  'Timetable'
  'Materialkitcount'
  'Material'
  ShowMaterialsheetsCtrl
]