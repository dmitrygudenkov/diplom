class NavCtrl
  constructor: ($scope, @$state, @$mdSidenav) ->

  go_to: (path) =>
    @$state.go path
    @$mdSidenav('menu').toggle()

  openLeftMenu: =>
    @$mdSidenav('menu').toggle()

angular.module('skyscraper').controller 'NavCtrl', [
  '$scope'
  '$state'
  '$mdSidenav'
  NavCtrl
]