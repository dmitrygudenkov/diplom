class PeriodCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'periods'


angular.module('skyscraper')
  .factory 'Period', [
    '$http'
    ($http) ->
      new PeriodCRUD($http)
]