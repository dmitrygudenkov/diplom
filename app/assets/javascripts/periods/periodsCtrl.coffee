class PeriodsCtrl
  constructor: ($scope, @$uibModal, @$log, @Building, @Period) ->
    @$scope = $scope
    @Period.all()
      .then (periods) =>
        periods.data.forEach (p) =>
          p.start_date = new Date p.start_date
          p.end_date = new Date p.end_date
          p.input_date = new Date p.input_date
        @periods = periods.data
        @Building.all()
      .then (buildings) =>
        @buildings = buildings.data
        @$scope.$digest() unless @$scope.$$phase

  showBuildingName: (id) =>
    if @buildings
      building = @buildings.find (b) =>
        if b.id == id
          b
      building.name

  check_data: (data) =>
    unless data
      return "Данные должны существовать"

  save: (period) =>
    @Period.update(period)
      .then (response) =>
        console.log "ok"
      .then (response) =>
        console.log "not ok"

  showWindow: =>
    modalInstace = @$uibModal.open(
      animation: true
      templateUrl: 'periods/windows/_addPeriod.html'
      controller: 'AddPeriodWindowCtrl as ctrl'
      size: 'lg')
    modalInstace.result.then (response) =>
      response.start_date = new Date response.start_date
      if response then @periods.push response

  destroy: (period) =>
    swal {
      title: 'Вы уверены?'
      text: 'Вы действительно хотите удалить ' + @showBuildingName(period.building_id) + '?'
      type: 'warning'
      showCancelButton: true
      confirmButtonClass: 'btn-warning'
      confirmButtonText: 'Да'
      closeOnConfirm: true
      closeOnCancel: true
    }, (isConfirm) =>
      if isConfirm
        @Period.destroy period
          .then (success) =>
            i = @periods.findIndex (elem) =>
              if elem.id == period.id
                elem
            @periods.splice i, 1
          .then (error) =>
            console.log error


angular.module('skyscraper').controller 'PeriodsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  'Building'
  'Period'
  PeriodsCtrl
]