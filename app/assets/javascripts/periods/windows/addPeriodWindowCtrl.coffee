class AddPeriodWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Building, @Period) ->
    @$scope = $scope
    @period = 
      building_id: null
      start_date:  null
      end_date:    null
      wait_total:  null
      metric:      null
      power:       null
      power_cost:  null
      input_date:  null
      own_total:   null
      sub_total:   null
    @Building.all()
      .then (response) =>
        @buildings = response.data
        @$scope.$digest() unless @$scope.$$phase


  addPeriod: =>
    for key, value of @period
      unless value
        swal 'Ошибка', 'Надо заполнить все поля!', 'error'
        return
    @period.start_date = new Date(@period.start_date.setDate(@period.start_date.getDate() + 1))
    @period.end_date = new Date(@period.end_date.setDate(@period.end_date.getDate() + 1))
    @period.input_date = new Date(@period.input_date.setDate(@period.input_date.getDate() + 1))
    @Period.create @period
      .then (success) =>
        @$uibModalInstance.close(success.data)
      .then (error) =>
        @$uibModalInstance.close()
      

  cancel: =>
    @$uibModalInstance.dismiss()




angular.module('skyscraper').controller 'AddPeriodWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Building'
  'Period'
  AddPeriodWindowCtrl
]