class ProfessioncountCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'professioncounts'

  find: (id) ->
    @$http.get '/' + @model + '?find=' + id

  group_all: ->
    @$http.get '/' + @model + '?sql=true'

angular.module('skyscraper')
  .factory 'Professioncount', [
    '$http'
    ($http) ->
      new ProfessioncountCRUD($http)
]