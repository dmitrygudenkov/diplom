class ProfessionCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'professions'


angular.module('skyscraper')
  .factory 'Profession', [
    '$http'
    ($http) ->
      new ProfessionCRUD($http)
]