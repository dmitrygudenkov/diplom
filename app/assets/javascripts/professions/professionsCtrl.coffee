class ProfessionsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @Profession) ->
    super $scope, @Profession, 'profession'

angular.module('skyscraper').controller 'ProfessionsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  'Profession'
  ProfessionsCtrl
]