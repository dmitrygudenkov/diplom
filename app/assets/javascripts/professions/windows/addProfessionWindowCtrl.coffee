class AddProfessionWindowCtrl extends AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Profession) ->
    super $scope, @$uibModalInstance, @Profession, 1

angular.module('skyscraper').controller 'AddProfessionWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Profession'
  AddProfessionWindowCtrl
]