class ShowProfessionsheetsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @$state, @$stateParams, @Timetable, @Professioncount, @Profession) ->
    super $scope, @Timetable, 'timetable'

  load_data: =>
    @Timetable.get_works(@$stateParams.id)
      .then (response) =>
        @works = response.data
        @Professioncount.group_all()
      .then (response) =>
        @teams = response.data
        @Profession.all()
      .then (response) =>
        @professions = response.data
        @$scope.$digest() unless @$scope.$$phase
        do @s_works

  go_to: (state) ->
    @$state.go state, id: @$stateParams.id, id1: @$stateParams.id

  s_works: =>
    @works.forEach (w) =>
      @get_start_day w
      @add_professions_to_works w
    @cal_professions = []
    @works.forEach (w) =>
      w.professions.forEach (p) =>
        coincedence = false
        @cal_professions.forEach (tp) =>
          if p.id == tp.id
            coincedence = true
            tp.dates.push { start_date: w.start_date, end_date: w.start_date + w.duration } 
            tp.counts.push p.count
        if coincedence == false
          p.dates = []
          p.counts = []
          p.dates.push { start_date: w.start_date, end_date: w.start_date + w.duration } 
          p.counts.push p.count
          @cal_professions.push p
    console.log @cal_professions


  get_start_day: (work) =>
      if work.prev_work_id == null
        work.start_date = 1
      else
        @works.forEach (w) =>
          if work.prev_work_id == w.id
            unless w.start_date == undefined
              work.start_date = w.start_date + w.duration
            else
              @get_start_day w
              work.start_date = w.start_date + w.duration

  add_professions_to_works: (work) =>
    work.professions = []
    @teams.forEach (t) =>
      if t.team_id == work.team_id
        @professions.forEach (p) =>
          if p.id == t.profession_id
            work.professions.push {id: p.id, name: p.name, count: t.count }
    work


angular.module('skyscraper').controller 'ShowProfessionsheetsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  '$state'
  '$stateParams'
  'Timetable'
  'Professioncount'
  'Profession'
  ShowProfessionsheetsCtrl
]