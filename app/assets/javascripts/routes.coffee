Routes = ($stateProvider, $urlRouterProvider) ->
  $stateProvider.state('home',
    url: '/home'
    templateUrl: 'home/_home.html'
    controller: 'MainCtrl')
  
  .state 'buildings',
    url: '/buildings'
    templateUrl: 'buildings/_buildings.html'

  .state 'periods',
    url: '/periods'
    templateUrl: 'periods/_periods.html'

  .state 'worktypes',
    url: '/worktypes'
    templateUrl: 'worktypes/_worktypes.html'

  .state 'machines',
    url: '/machines'
    templateUrl: 'machines/_machines.html'

  .state 'materials',
    url: '/materials'
    templateUrl: 'materials/_materials.html'

  .state 'professions',
    url: '/professions'
    templateUrl: 'professions/_professions.html'

  .state 'contractors',
    url: '/contractors'
    templateUrl: 'contractors/_contractors.html'

  .state 'machinekits',
    url: '/machinekits'
    templateUrl: 'machinekits/_machinekits.html'

  .state 'materialkits',
    url: '/materialkits'
    templateUrl: 'materialkits/_materialkits.html'

  .state 'teams',
    url: '/teams'
    templateUrl: 'teams/_teams.html'

  .state 'timetables',
    url: '/timetables'
    templateUrl: 'timetables/_timetables.html'

  .state 'timetable',
    url: '/timetables/:id'
    templateUrl: 'timetables/_showTimetable.html'

  .state 'workcalculation',
    url: '/timetables/:id/workcalculations/:id1'
    templateUrl: 'workcalculations/_showWorkcalculation.html'

  .state 'worksheet',
    url: '/timetables/:id/worksheets/:id1'
    templateUrl: 'worksheets/_showWorksheet.html'

  .state 'event',
    url: '/timetables/:id/event/:id1'
    templateUrl: 'events/_showEvent.html'

  .state 'materialcalculation',
    url: '/timetables/:id/materialcalculation/:id1'
    templateUrl: 'materialcalculations/_showMaterialcalculation.html'

  .state 'machinesheet',
    url: '/timetables/:id/machinesheet/:id1'
    templateUrl: 'machinesheets/_showMachinesheet.html'

  .state 'professionsheet',
    url: '/timetables/:id/professionsheet/:id1'
    templateUrl: 'professionsheets/_showProfessionsheet.html'

  .state 'materialsheet',
    url: '/timetables/:id/materialsheet/:id1'
    templateUrl: 'materialsheets/_showMaterialsheet.html'

  .state 'success_work',
    url: '/timetables/:id/success_work/:id1'
    templateUrl: 'success_works/_showSuccessWork.html'
    
  $urlRouterProvider.otherwise 'home'


angular.module('skyscraper').config [
  '$stateProvider'
  '$urlRouterProvider'
  Routes
]