class ShowSuccessWorkCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @$stateParams, @Worktype, @Work, @Contractor) ->
    super $scope, @Work, 'work'

  load_data: =>
    @Worktype.all()
      .then (success) =>
        @worktypes = success.data
        @Work.find(@$stateParams.id)
      .then (success) =>
        @data = success.data
        console.log @data
        @$scope.$digest() unless @$scope.$$phase
        do @bind_data

  bind_data: =>
    @data.forEach (d) =>
      @bind_data_for_row d

  bind_data_for_row: (r) =>
    r.worktype = @worktypes.find (wt) =>
      if r.worktype_id == wt.id
        wt
    console.log r

  save: (row) =>
    @Work.update row
      .then (success) =>

angular.module('skyscraper').controller 'ShowSuccessWorkCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  '$stateParams'
  'Worktype'
  'Work'
  'Contractor'
  ShowSuccessWorkCtrl
]