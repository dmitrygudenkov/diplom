class TeamCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'teams'


angular.module('skyscraper')
  .factory 'Team', [
    '$http'
    ($http) ->
      new TeamCRUD($http)
]