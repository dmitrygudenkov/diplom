class TeamsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @Team) ->
    super $scope, @Team, 'team'

  editKit: (team) ->
    modalInstace = @$uibModal.open(
      animation: true
      resolve: {
        team: -> return team
      }
      templateUrl: 'teams/windows/_showTeam.html'
      controller: 'ShowTeamWindowCtrl as ctrl'
      size: 'lg')

angular.module('skyscraper').controller 'TeamsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  'Team'
  TeamsCtrl
]