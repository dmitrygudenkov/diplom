class AddTeamWindowCtrl extends AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Team) ->
    super $scope, @$uibModalInstance, @Team, 1

angular.module('skyscraper').controller 'AddTeamWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Team'
  AddTeamWindowCtrl
]