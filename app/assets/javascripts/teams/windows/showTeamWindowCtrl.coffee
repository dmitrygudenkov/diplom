class @ShowTeamWindowCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModalInstance, @Professioncount, @Profession, @team) ->
    super $scope, @Professioncount, 'professioncount'
    @$scope = $scope
    do @load_data

  load_data: =>
    @Professioncount.find(@team.id)
      .then (response) =>
        @data = response.data
        @Profession.all()
      .then (response) =>
        @professions = response.data
        @$scope.$digest() unless @$scope.$$phase

  showName: (id) =>
    if @professions
      profession = @professions.find (m) =>
        if m.id == id
          m
      profession.name

  add: =>
    profession = 
      team_id:       @team.id
      profession_id: @professions[0].id
      count:         1
    @Professioncount.create profession
      .then (success) =>
        @data.push success.data




angular.module('skyscraper').controller 'ShowTeamWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Professioncount'
  'Profession'
  'team'
  ShowTeamWindowCtrl
]