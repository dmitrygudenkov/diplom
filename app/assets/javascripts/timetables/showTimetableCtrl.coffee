class ShowTimetableCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @$state, @$stateParams, @Timetable, @Machinekitcount, @Professioncount, @Machine, @Profession) ->
    super $scope, @Timetable, 'timetable'
  load_data: =>
    @Timetable.get_works(@$stateParams.id)
      .then (response) =>
        @works = response.data
        @Machinekitcount.group_all()
      .then (response) =>
        @machinekits = response.data
        @Professioncount.group_all()
      .then (response) =>
        @teams = response.data
        @Machine.all()
      .then (response) =>
        @machines = response.data
        @Profession.all()
      .then (response) =>
        @professions = response.data
        @$scope.$digest() unless @$scope.$$phase
        do @s_works

  go_to: (state) ->
    @$state.go state, id: @$stateParams.id, id1: @$stateParams.id

  s_works: =>
    @works.forEach (w) =>
      @get_start_day w
      @add_machines_and_professions_to_works w

  get_start_day: (work) =>
      if work.prev_work_id == null
        work.start_date = 1
      else
        @works.forEach (w) =>
          if work.prev_work_id == w.id
            unless w.start_date == undefined
              work.start_date = w.start_date + w.duration
            else
              @get_start_day w
              work.start_date = w.start_date + w.duration

  add_machines_and_professions_to_works: (work) =>
    work.machines = []
    @machinekits.forEach (mk) =>
      if mk.machinekit_id == work.machinekit_id
        @machines.forEach (m) =>
          if m.id == mk.machine_id
            mk.name = m.name
            mk.perfomance = m.perfomance
            work.machines.push mk
    work.professions = []
    @teams.forEach (t) =>
      if t.team_id == work.team_id
        @professions.forEach (p) =>
          if p.id == t.profession_id
            t.name = p.name
            work.professions.push t
    work

  get_count: (array) =>
    count = 0
    array.forEach (a) =>
      count += a.count
    count


angular.module('skyscraper').controller 'ShowTimetableCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  '$state'
  '$stateParams'
  'Timetable'
  'Machinekitcount'
  'Professioncount'
  'Machine'
  'Profession'
  ShowTimetableCtrl
]