class TimetableCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'timetables'

  get_works: (id) =>
    @$http.get '/' + @model + '/' + id + '?query=true'

angular.module('skyscraper')
  .factory 'Timetable', [
    '$http'
    ($http) ->
      new TimetableCRUD($http)
]