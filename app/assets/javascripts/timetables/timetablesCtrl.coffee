class TimetablesCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @$state, @Timetable, @Building) ->
    super $scope, @Timetable, 'timetable'

  load_data: =>
    @Model.all()
      .then (response) =>
        @data = response.data
        @Building.all()
      .then (response) =>
        @buildings = response.data
        @$scope.$digest() unless @$scope.$$phase

  go_to: (id) ->
    @$state.go 'timetable', id: id

  show_name: (id) =>
    building = @buildings.find (b) =>
      if id == b.id
        b
    building.name

angular.module('skyscraper').controller 'TimetablesCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  '$state'
  'Timetable'
  'Building'
  TimetablesCtrl
]