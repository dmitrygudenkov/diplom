class AddTimetableWindowCtrl extends AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Timetable, @Building) ->
    super $scope, @$uibModalInstance, @Timetable, 2
    do @load_data

  load_data: =>
    @Building.all()
      .then (response) =>
        @buildings = response.data

angular.module('skyscraper').controller 'AddTimetableWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Timetable'
  'Building'
  AddTimetableWindowCtrl
]