class ShowWorkcalculationsCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @$stateParams, @Work, @Worktype, @Profession, @Machine, @Machinekitcount, @Professioncount, @Event) ->
    super $scope, @Work, 'work'

  load_data: =>
    @Work.find(@$stateParams.id)
      .then (success) =>
        @data = success.data
        @Worktype.all()
      .then (success) =>
        @worktypes = success.data
        @Profession.all()
      .then (success) =>
        @professions = success.data
        @Machine.all()
      .then (success) =>
        @machines = success.data
        @Machinekitcount.group_all()
      .then (success) =>
        @machinekits = success.data
        @Professioncount.group_all()
      .then (success) =>
        @teams = success.data
        @Event.all()
      .then (success) =>
        @events = success.data
        @$scope.$digest() unless @$scope.$$phase
        do @bind_data

  bind_data: =>
    @data.forEach (d) =>
      @bind_data_for_row d

  bind_data_for_row: (r) =>
    r.worktype = @worktypes.find (wt) =>
      if r.worktype_id == wt.id
        wt
    r.professions = []
    @teams.forEach (t) =>
      if r.team_id == t.team_id
        @professions.forEach (p) =>
          if t.profession_id == p.id
            p.count = t.count
            r.professions.push p
    r.machines = []
    @machinekits.forEach (mk) =>
      if mk.machinekit_id == r.machinekit_id
        @machines.forEach (m) =>
          if mk.machine_id == m.id
            mk.name = m.name
            mk.perfomance = m.perfomance
            r.machines.push mk

  showWindow: =>
    modalInstace = @$uibModal.open(
      animation: true
      templateUrl: 'workcalculations/windows/_addWorkcalculationRow.html'
      controller: 'AddWorkcalculationRowWindowCtrl as ctrl'
      size: 'lg')
    modalInstace.result.then (response) =>
      if response
       @bind_data_for_row response
       @data.push response

  destroy: (elem) =>
    super
    event = @events.find (e) =>
      if e.work_id == elem.id
        e
    @Event.destroy event
      .then (success) =>

angular.module('skyscraper').controller 'ShowWorkcalculationsCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  '$stateParams'
  'Work'
  'Worktype'
  'Profession' 
  'Machine'
  'Machinekitcount'
  'Professioncount'
  'Event'
  ShowWorkcalculationsCtrl
]