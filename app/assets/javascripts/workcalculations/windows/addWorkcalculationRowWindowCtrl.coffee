class AddWorkcalculationRowWindowCtrl extends AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @$stateParams, @Worktype, @Machinekit, @Materialkit, @Contractor, @Team, @Work, @Event) ->
    super $scope, @$uibModalInstance, @Work, 8
    do @load_data

  load_data: =>
    @Worktype.all()
      .then (success) =>
        @worktypes = success.data
        @Contractor.all()
      .then (success) =>
        @contractors = success.data
        @Machinekit.all()
      .then (success) =>
        @machinekits = success.data
        @Team.all()
      .then (success) =>
        @teams = success.data
        @Materialkit.all()
      .then (success) =>
        @materialkits = success.data
        @$scope.$digest() unless @$scope.$$phase

  add: =>
    unless @data
      swal 'Ошибка', 'Надо заполнить все поля!!', 'error'
      return
    @data.timetable_id  = parseInt @$stateParams.id
    @data.volume        = 1
    @data.cost          = 1
    @data.contractor_id = @contractors[0].id
    keysCount = 0
    for key, value of @data
      keysCount++
      unless value
        swal 'Ошибка', 'Надо заполнить все поля!!', 'error'
        return
    if keysCount != @keysCount
      swal 'Ошибка', 'Надо заполнить все поля!', 'error'
      return
    @Model.create @data
      .then (success) =>
        @work = success.data
        event = 
          timetable_id: parseInt @$stateParams.id
          work_id:      success.data.id
          prev_work_id: null
          duration:     1
        @Event.create event
      .then (success) =>
        @$uibModalInstance.close(@work)
      .then (error) =>
        @$uibModalInstance.close()


angular.module('skyscraper').controller 'AddWorkcalculationRowWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  '$stateParams'
  'Worktype'
  'Machinekit'
  'Materialkit'
  'Contractor'
  'Team'
  'Work'
  'Event'
  AddWorkcalculationRowWindowCtrl
]