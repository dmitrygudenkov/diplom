class WorkCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'works'

  find: (id) ->
    @$http.get '/' + @model + '?find=' + id

angular.module('skyscraper')
  .factory 'Work', [
    '$http'
    ($http) ->
      new WorkCRUD($http)
]