class AddWorktypeWindowCtrl extends AbstractWindowCtrl
  constructor: ($scope, @$uibModalInstance, @Worktype) ->
    super $scope, @$uibModalInstance, @Worktype, 5

angular.module('skyscraper').controller 'AddWorktypeWindowCtrl', [
  '$scope'
  '$uibModalInstance'
  'Worktype'
  AddWorktypeWindowCtrl
]