class WorktypeCRUD extends AbstractFactory
  constructor: ($http) ->
    super $http, 'worktypes'


angular.module('skyscraper')
  .factory 'Worktype', [
    '$http'
    ($http) ->
      new WorktypeCRUD($http)
]