class WorktypesCtrl extends AbstractDirCtrl
  constructor: ($scope, @$uibModal, @$log, @Worktype) ->
    super $scope, @Worktype, 'worktype'

angular.module('skyscraper').controller 'WorktypesCtrl', [
  '$scope'
  '$uibModal'
  '$log'
  'Worktype'
  WorktypesCtrl
]