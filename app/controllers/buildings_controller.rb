class BuildingsController < ApplicationController

  before_action :find_building, only: [:show, :update, :destroy]

  def index
    respond_with Building.all
  end

  def create
    respond_with Building.create(building_params)
  end

  def show
    respond_with @building
  end

  def update
    @building.update_attributes(building_params)
    respond_with @building
  end

  def destroy
    respond_with @building.destroy
  end

  private
    def building_params
      params.require(:building).permit(:name, :full_cost, :smr_cost, :start_date)
    end

    def find_building
      @building = Building.find(params[:id])
    end
end
