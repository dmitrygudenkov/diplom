class ContractorsController < ApplicationController

  def index
    respond_with Contractor.all
  end

  def create
    respond_with Contractor.create(contractor_params)
  end

  def show
    respond_with Contractor.find(params[:id])
  end

  def update
    @contractor = Contractor.find(params[:id])
    @contractor.update_attributes(contractor_params)
    respond_with @contractor
  end

  def destroy
    respond_with Contractor.find(params[:id]).destroy
  end

  private
    def contractor_params
      params.require(:contractor).permit(:name)
    end

end
