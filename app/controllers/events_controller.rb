class EventsController < ApplicationController

  def index
    @event = Event.all
    if params[:find]
      @event = @event.where('timetable_id = ?', params[:find])
    end
    render json: @event
  end

  def create
    respond_with Event.create(event_params)
  end

  def show
    respond_with Event.find(params[:id])
  end

  def update
    @event = Event.find(params[:id])
    @event.update_attributes(event_params)
    respond_with @event
  end

  def destroy
    respond_with Event.find(params[:id]).destroy
  end

  private
    def event_params
      params.require(:event).permit(:timetable_id, :work_id, :prev_work_id, :duration)
    end

end
