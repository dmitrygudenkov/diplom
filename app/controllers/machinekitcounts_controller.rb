class MachinekitcountsController < ApplicationController

  def index
    @machinekitcount = Machinekitcount.all
    if params[:find]
      @machinekitcount = @machinekitcount.where('machinekit_id = ?', params[:find])
    end
    if params[:sql]
      @machinekitcount = Machinekitcount.find_by_sql('select id, machinekit_id, machine_id, sum(count) count from machinekitcounts group by machinekit_id, machine_id')
    end
    render json: @machinekitcount #Не вышло чтобы .json не приходил в params
  end

  def create
    respond_with Machinekitcount.create(machinekitcount_params)
  end

  def show
    respond_with Machinekitcount.find(params[:id])
  end

  def update
    @machinekitcount = Machinekitcount.find(params[:id])
    @machinekitcount.update_attributes(machinekitcount_params)
    respond_with @machinekitcount
  end

  def destroy
    respond_with Machinekitcount.find(params[:id]).destroy
  end

  private
    def machinekitcount_params
      params.require(:machinekitcount).permit(:machinekit_id, :machine_id, :count)
    end
  
end
