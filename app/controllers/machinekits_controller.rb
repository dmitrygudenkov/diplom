class MachinekitsController < ApplicationController

  def index
    respond_with Machinekit.all
  end

  def create
    respond_with Machinekit.create(machinekit_params)
  end

  def show
    respond_with Machinekit.find(params[:id])
  end

  def update
    @machinekit = Machinekit.find(params[:id])
    @machinekit.update_attributes(machinekit_params)
    respond_with @machinekit
  end

  def destroy
    respond_with Machinekit.find(params[:id]).destroy
  end

  private
    def machinekit_params
      params.require(:machinekit).permit(:name)
    end

end
