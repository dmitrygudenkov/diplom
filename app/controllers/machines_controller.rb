class MachinesController < ApplicationController

  def index
    respond_with Machine.all
  end

  def create
    respond_with Machine.create(machine_params)
  end

  def show
    respond_with Machine.find(params[:id])
  end

  def update
    @machine = Machine.find(params[:id])
    @machine.update_attributes(machine_params)
    respond_with @machine
  end

  def destroy
    respond_with Machine.find(params[:id]).destroy
  end

  private
    def machine_params
      params.require(:machine).permit(:name, :perfomance)
    end

end
