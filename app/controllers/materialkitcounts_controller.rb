class MaterialkitcountsController < ApplicationController

  def index
    @materialkitcount = Materialkitcount.all
    if params[:find]
      @materialkitcount = @materialkitcount.where('materialkit_id = ?', params[:find])
    end
    if params[:sql]
      @materialkitcount = Materialkitcount.find_by_sql('select id, materialkit_id, material_id, sum(norm)/count(norm) norm from materialkitcounts group by materialkit_id, material_id')
    end
    render json: @materialkitcount
  end

  def create
    respond_with Materialkitcount.create(materialkitcount_params)
  end

  def show
    respond_with Materialkitcount.find(params[:id])
  end

  def update
    @materialkitcount = Materialkitcount.find(params[:id])
    @materialkitcount.update_attributes(materialkitcount_params)
    respond_with @materialkitcount
  end

  def destroy
    respond_with Materialkitcount.find(params[:id]).destroy
  end

  private
    def materialkitcount_params
      params.require(:materialkitcount).permit(:materialkit_id, :material_id, :norm)
    end

end
