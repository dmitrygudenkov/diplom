class MaterialkitsController < ApplicationController

  def index
    respond_with Materialkit.all
  end

  def create
    respond_with Materialkit.create(materialkit_params)
  end

  def show
    respond_with Materialkit.find(params[:id])
  end

  def update
    @materialkit = Materialkit.find(params[:id])
    @materialkit.update_attributes(materialkit_params)
    respond_with @materialkit
  end

  def destroy
    respond_with Materialkit.find(params[:id]).destroy
  end

  private
    def materialkit_params
      params.require(:materialkit).permit(:name)
    end

end
