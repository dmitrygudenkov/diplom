class MaterialsController < ApplicationController

  def index
    respond_with Material.all
  end

  def create
    respond_with Material.create(material_params)
  end

  def show
    respond_with Material.find(params[:id])
  end

  def update
    @material = Material.find(params[:id])
    @material.update_attributes(material_params)
    respond_with @material
  end

  def destroy
    respond_with Material.find(params[:id]).destroy
  end

  private
    def material_params
      params.require(:material).permit(:name, :metric)
    end

end
