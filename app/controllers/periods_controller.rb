class PeriodsController < ApplicationController

  def index
    respond_with Period.all
  end

  def create
    respond_with Period.create(period_params)
  end

  def show
    respond_with Period.find(params[:id])
  end

  def update
    @period = Period.find(params[:id])
    @period.update_attributes(period_params)
    respond_with @period
  end

  def destroy
    respond_with Period.find(params[:id]).destroy
  end

  private
    def period_params
      params.require(:period).permit(:start_date, :end_date, :wait_total, :metric, :power, :power_cost, :input_date, :own_total, :sub_total, :building_id)
    end

end
