class ProfessioncountsController < ApplicationController

  def index
    @professioncount = Professioncount.all
    if params[:find]
      @professioncount = @professioncount.where('team_id = ?', params[:find])
    end
    if params[:sql]
      @professioncount = Professioncount.find_by_sql('select id, team_id, profession_id, sum(count) count from professioncounts group by team_id, profession_id')
    end
    render json: @professioncount #Не вышло чтобы .json не приходил в params
  end

  def create
    respond_with Professioncount.create(professioncount_params)
  end

  def show
    respond_with Professioncount.find(params[:id])
  end

  def update
    @professioncount = Professioncount.find(params[:id])
    @professioncount.update_attributes(professioncount_params)
    respond_with @professioncount
  end

  def destroy
    respond_with Professioncount.find(params[:id]).destroy
  end

  private
    def professioncount_params
      params.require(:professioncount).permit(:team_id, :profession_id, :count)
    end

end
