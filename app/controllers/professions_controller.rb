class ProfessionsController < ApplicationController

  def index
    respond_with Profession.all
  end

  def create
    respond_with Profession.create(profession_params)
  end

  def show
    respond_with Profession.find(params[:id])
  end

  def update
    @profession = Profession.find(params[:id])
    @profession.update_attributes(profession_params)
    respond_with @profession
  end

  def destroy
    respond_with Profession.find(params[:id]).destroy
  end

  private
    def profession_params
      params.require(:profession).permit(:name)
    end

end
