class TeamsController < ApplicationController

  def index
    respond_with Team.all
  end

  def create
    respond_with Team.create(team_params)
  end

  def show
    respond_with Team.find(params[:id])
  end

  def update
    @team = Team.find(params[:id])
    @team.update_attributes(team_params)
    respond_with @team
  end

  def destroy
    respond_with Team.find(params[:id]).destroy
  end

  private
    def team_params
      params.require(:team).permit(:name)
    end

end
