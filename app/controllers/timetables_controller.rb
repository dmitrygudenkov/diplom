class TimetablesController < ApplicationController

  def index
    @timetable = Timetable.all
    respond_with @timetable
  end

  def create
    respond_with Timetable.create(timetable_params)
  end

  def show
    @timetable = Timetable.find(params[:id])
    if params[:query]
      @timetable = Timetable.find_by_sql(
        'select w.id, e.prev_work_id, wt.name, wt.labor_expenses, c.name as contractor, wt.metric, w.volume, w.success, wt.man_days, machinekit_id, e.duration, w.team_id, w.materialkit_id 
         from works w 
         join worktypes wt on w.worktype_id = wt.id 
         join contractors c on w.contractor_id = c.id 
         join events e on w.id = e.work_id 
         where w.timetable_id = ' + params[:id]
      )
    end
    render json: @timetable
  end

  def update
    @timetable = Timetable.find(params[:id])
    @timetable.update_attributes(timetable_params)
    respond_with @timetable
  end

  def destroy
    respond_with Timetable.find(params[:id]).destroy
  end

  private
    def timetable_params
      params.require(:timetable).permit(:name, :building_id)
    end

end
