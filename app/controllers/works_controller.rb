class WorksController < ApplicationController

  def index
    @work = Work.all
    if params[:find]
      @work = @work.where('timetable_id = ?', params[:find])
    end
    render json: @work
  end

  def create
    respond_with Work.create(work_params)
  end

  def show
    respond_with Work.find(params[:id])
  end

  def update
    @work = Work.find(params[:id])
    @work.update_attributes(work_params)
    respond_with @work
  end

  def destroy
    respond_with Work.find(params[:id]).destroy
  end

  private
    def work_params
      params.require(:work).permit(:worktype_id, :volume, :cost, :contractor_id, :timetable_id, :team_id, :machinekit_id, :materialkit_id, :success)
    end

end
