class WorktypesController < ApplicationController
  
  def index
    respond_with Worktype.all
  end

  def create
    respond_with Worktype.create(work_params)
  end

  def show
    respond_with Worktype.find(params[:id])
  end

  def update
    @work = Worktype.find(params[:id])
    @work.update_attributes(work_params)
    respond_with @work
  end

  def destroy
    respond_with Worktype.find(params[:id]).destroy
  end

  private
    def work_params
      params.require(:worktype).permit(:name, :metric, :enir, :man_days, :labor_expenses)
    end
end
