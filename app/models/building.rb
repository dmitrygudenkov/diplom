class Building < ActiveRecord::Base
  has_many :periods

  validates :name, presence: true
  validates :full_cost, :smr_cost, numericality: { greater_than: 0, allow_nil: false } 
end
