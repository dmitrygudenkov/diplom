class Machine < ActiveRecord::Base
  has_many :machinekitcounts
  has_many :machinekits, through: :machinekitcounts
end
