class Machinekit < ActiveRecord::Base

  has_many :machinekitcounts
  has_many :machines, through: :machinekitcounts
  has_many :workcalculationrelations
  has_many :workcalculation, through: :workcalculationrelations
  
end
