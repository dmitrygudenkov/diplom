class Material < ActiveRecord::Base

  has_many :materialkitcounts
  has_many :materialkits, through: :materialkitcounts

end
