class Materialkit < ActiveRecord::Base

  has_many :materialkitcounts
  has_many :materials, through: :materialkitcounts

end
