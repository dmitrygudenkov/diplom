class Profession < ActiveRecord::Base

  has_many :professioncounts
  has_many :teams, through: :professioncounts

end
