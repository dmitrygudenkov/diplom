class Team < ActiveRecord::Base

  has_many :professioncounts
  has_many :professions, through: :professioncounts
  has_many :workcalculationrelations
  has_many :workcalculation, through: :workcalculationrelations

end
