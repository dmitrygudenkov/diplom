class Work < ActiveRecord::Base

  belongs_to :worktype
  belongs_to :contractor
  belongs_to :worksheet
  has_many   :events

end
