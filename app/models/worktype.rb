class Worktype < ActiveRecord::Base

  has_many :workcalculationrelations
  has_many :workcalculation, through: :workcalculationrelations
  has_many :works
  
end
