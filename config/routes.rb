Rails.application.routes.draw do
  root to: 'application#angular'

  resources :buildings, only: [:create, :index, :show, :update, :destroy]
  resources :periods,   only: [:create, :index, :show, :update, :destroy]
  resources :worktypes, only: [:create, :index, :show, :update, :destroy]
  resources :machines,  only: [:create, :index, :show, :update, :destroy]
  resources :materials,  only: [:create, :index, :show, :update, :destroy]
  resources :professions,  only: [:create, :index, :show, :update, :destroy]
  resources :contractors,  only: [:create, :index, :show, :update, :destroy]
  resources :machinekits,  only: [:create, :index, :show, :update, :destroy]
  resources :machinekitcounts, only: [:create, :index, :show, :update, :destroy]
  resources :materialkits,  only: [:create, :index, :show, :update, :destroy]
  resources :materialkitcounts, only: [:create, :index, :show, :update, :destroy]
  resources :teams,  only: [:create, :index, :show, :update, :destroy]
  resources :professioncounts, only: [:create, :index, :show, :update, :destroy]
  resources :works, only: [:create, :index, :show, :update, :destroy]
  resources :events, only: [:create, :index, :show, :update, :destroy]
  resources :timetables, only: [:create, :index, :show, :update, :destroy]
end
