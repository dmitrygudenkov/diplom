class CreateBuildings < ActiveRecord::Migration
  def change
    create_table :buildings do |t|
      t.string :name
      t.float  :full_cost
      t.float  :smr_cost
      t.date   :start_date

      t.timestamps null: false
    end
  end
end
