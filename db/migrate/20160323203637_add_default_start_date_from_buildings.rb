class AddDefaultStartDateFromBuildings < ActiveRecord::Migration
  def change
    change_column :buildings, :start_date, :date, default: Time.now
  end
end
