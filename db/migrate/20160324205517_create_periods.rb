class CreatePeriods < ActiveRecord::Migration
  def change
    create_table :periods do |t|
      t.date       :start_date
      t.date       :end_date
      t.float      :wait_total
      t.string     :metric
      t.float      :power
      t.float      :power_cost
      t.date       :input_date
      t.float      :own_total
      t.float      :sub_total
      t.references :building, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
