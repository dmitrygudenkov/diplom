class CreateWorktypes < ActiveRecord::Migration
  def change
    create_table :worktypes do |t|
      t.string :name
      t.string :metric
      t.string :enir
      t.float  :man_days
      

      t.timestamps null: false
    end
  end
end
