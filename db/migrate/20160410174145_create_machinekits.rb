class CreateMachinekits < ActiveRecord::Migration
  def change
    create_table :machinekits do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
