class CreateMachinekitcounts < ActiveRecord::Migration
  def change
    create_table :machinekitcounts do |t|
      t.references :machinekit, foreign_key: true
      t.references :machine, foreign_key: true
      t.integer    :count
      t.float      :labor_expenses

      t.timestamps null: false
    end
  end
end
