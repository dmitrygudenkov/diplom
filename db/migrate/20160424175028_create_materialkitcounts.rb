class CreateMaterialkitcounts < ActiveRecord::Migration
  def change
    create_table :materialkitcounts do |t|
      t.references :materialkit, foreign_key: true
      t.references :material, foreign_key: true
      t.integer    :norm

      t.timestamps null: false
    end
  end
end
