class CreateProfessioncounts < ActiveRecord::Migration
  def change
    create_table :professioncounts do |t|
      t.references :team, foreign_key: true
      t.references :profession, foreign_key: true
      t.integer    :count

      t.timestamps null: false
    end
  end
end
