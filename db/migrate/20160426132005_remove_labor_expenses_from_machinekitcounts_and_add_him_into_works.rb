class RemoveLaborExpensesFromMachinekitcountsAndAddHimIntoWorks < ActiveRecord::Migration
  def change
    remove_column :machinekitcounts, :labor_expenses
    add_column    :worktypes, :labor_expenses, :integer
  end
end
