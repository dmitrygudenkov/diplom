class AddColumnsIntoWorkcalculationrelation < ActiveRecord::Migration
  def change
    add_column :workcalculationrelations, :workcalculation_id, :integer, references: :workcalculations
    add_column :workcalculationrelations, :work_id, :integer, references: :works
    add_column :workcalculationrelations, :team_id, :integer, references: :teams
    add_column :workcalculationrelations, :machinekit_id, :integer, references: :machinekits
  end
end
