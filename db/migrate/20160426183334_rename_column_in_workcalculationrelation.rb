class RenameColumnInWorkcalculationrelation < ActiveRecord::Migration
  def change
    rename_column :workcalculationrelations, :work_id, :worktype_id
  end
end
