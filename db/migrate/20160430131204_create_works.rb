class CreateWorks < ActiveRecord::Migration
  def change
    create_table :works do |t|
      # t.references :worktypes, foreign_key: true
      t.float      :volume
      t.float      :cost
      # t.references :contractors, foreign_key: true

      t.timestamps null: false
    end
  end
end
