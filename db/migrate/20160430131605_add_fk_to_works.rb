class AddFkToWorks < ActiveRecord::Migration
  def change
    add_column :works, :worktype_id, :integer, references: :worktypes
    add_column :works, :contractor_id, :integer, references: :contractors
  end
end
