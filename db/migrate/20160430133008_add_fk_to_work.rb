class AddFkToWork < ActiveRecord::Migration
  def change
    add_column :works, :worksheet_id, :integer, references: :worksheets
  end
end
