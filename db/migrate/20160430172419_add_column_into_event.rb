class AddColumnIntoEvent < ActiveRecord::Migration
  def change
    add_column :events, :worksheet_id, :integer, references: :worksheets
    add_column :events, :work_id, :integer, references: :works
    add_column :events, :next_work_id, :integer, references: :works
    add_column :events, :duration, :integer
  end
end
