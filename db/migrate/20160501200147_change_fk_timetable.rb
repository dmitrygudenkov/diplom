class ChangeFkTimetable < ActiveRecord::Migration
  def change
    rename_column :workcalculationrelations, :workcalculation_id, :timetable_id
    rename_column :works, :worksheet_id, :timetable_id
    rename_column :events, :worksheet_id, :timetable_id
  end
end
