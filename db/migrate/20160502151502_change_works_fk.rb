class ChangeWorksFk < ActiveRecord::Migration
  def change
    rename_column :works, :worktype_id, :workcalculationrelation_id
  end
end
