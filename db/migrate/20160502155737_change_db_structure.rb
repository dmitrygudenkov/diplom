class ChangeDbStructure < ActiveRecord::Migration
  def change
    rename_column :works, :workcalculationrelation_id, :worktype_id
    add_column    :works, :team_id, :integer, references: :teams
    add_column    :works, :machinekit_id, :integer, references: :machinekits

    drop_table    :workcalculations
    drop_table    :workcalculationrelations
    drop_table    :worksheets
  end
end
