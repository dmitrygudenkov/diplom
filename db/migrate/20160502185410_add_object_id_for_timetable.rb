class AddObjectIdForTimetable < ActiveRecord::Migration
  def change
    add_column :timetables, :building_id, :integer, references: :buildings
  end
end
