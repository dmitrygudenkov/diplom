class RenameColumnInEvents < ActiveRecord::Migration
  def change
    rename_column :events, :next_work_id, :prev_work_id
  end
end
