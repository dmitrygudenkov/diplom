class AddMaterialkitsIntoWorks < ActiveRecord::Migration
  def change
    add_column :works, :materialkit_id, :integer, references: :materialkits
  end
end
