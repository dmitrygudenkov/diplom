class ChangeColumnTypeInWorktypes < ActiveRecord::Migration
  def change
    change_column :worktypes, :man_days, :float
    change_column :worktypes, :labor_expenses, :float
    change_column :materialkitcounts, :norm, :float
  end
end
