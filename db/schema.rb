# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160612182101) do

  create_table "buildings", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.float    "full_cost",  limit: 24
    t.float    "smr_cost",   limit: 24
    t.date     "start_date",             default: '2016-04-26'
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  create_table "contractors", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "events", force: :cascade do |t|
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "timetable_id", limit: 4
    t.integer  "work_id",      limit: 4
    t.integer  "prev_work_id", limit: 4
    t.integer  "duration",     limit: 4
  end

  create_table "machinekitcounts", force: :cascade do |t|
    t.integer  "machinekit_id", limit: 4
    t.integer  "machine_id",    limit: 4
    t.integer  "count",         limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "machinekitcounts", ["machine_id"], name: "fk_rails_3820604f59", using: :btree
  add_index "machinekitcounts", ["machinekit_id"], name: "fk_rails_44bc730df6", using: :btree

  create_table "machinekits", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "machines", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.float    "perfomance", limit: 24
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "materialkitcounts", force: :cascade do |t|
    t.integer  "materialkit_id", limit: 4
    t.integer  "material_id",    limit: 4
    t.float    "norm",           limit: 24
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "materialkitcounts", ["material_id"], name: "fk_rails_f4dd7e8698", using: :btree
  add_index "materialkitcounts", ["materialkit_id"], name: "fk_rails_721cdd8900", using: :btree

  create_table "materialkits", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "materials", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "metric",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "periods", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.float    "wait_total",  limit: 24
    t.string   "metric",      limit: 255
    t.float    "power",       limit: 24
    t.float    "power_cost",  limit: 24
    t.date     "input_date"
    t.float    "own_total",   limit: 24
    t.float    "sub_total",   limit: 24
    t.integer  "building_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "periods", ["building_id"], name: "index_periods_on_building_id", using: :btree

  create_table "professioncounts", force: :cascade do |t|
    t.integer  "team_id",       limit: 4
    t.integer  "profession_id", limit: 4
    t.integer  "count",         limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "professioncounts", ["profession_id"], name: "fk_rails_911ac0792f", using: :btree
  add_index "professioncounts", ["team_id"], name: "fk_rails_d49d071e37", using: :btree

  create_table "professions", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "teams", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "timetables", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "building_id", limit: 4
  end

  create_table "works", force: :cascade do |t|
    t.float    "volume",         limit: 24
    t.float    "cost",           limit: 24
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "worktype_id",    limit: 4
    t.integer  "contractor_id",  limit: 4
    t.integer  "timetable_id",   limit: 4
    t.integer  "team_id",        limit: 4
    t.integer  "machinekit_id",  limit: 4
    t.integer  "materialkit_id", limit: 4
    t.float    "success",        limit: 24
  end

  create_table "worktypes", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "metric",         limit: 255
    t.string   "enir",           limit: 255
    t.float    "man_days",       limit: 24
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.float    "labor_expenses", limit: 24
  end

  add_foreign_key "machinekitcounts", "machinekits"
  add_foreign_key "machinekitcounts", "machines"
  add_foreign_key "materialkitcounts", "materialkits"
  add_foreign_key "materialkitcounts", "materials"
  add_foreign_key "periods", "buildings"
  add_foreign_key "professioncounts", "professions"
  add_foreign_key "professioncounts", "teams"
end
